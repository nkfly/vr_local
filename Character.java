import java.lang.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;


enum Direction {
    Up, Down, Left, Right
}
enum WalkPose {
    Still, LeftLeg, RightLeg
  }

class AdapterDemo extends WindowAdapter {
	public void windowClosing(WindowEvent e) {
		System.exit(0);
    }
}

class Frame extends JFrame implements CharacterWalkable, SoundSourceControllable{
	Character mainStar;
	SoundSource ss;

	
	public Frame(){
		setTitle("Test");
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(800, 600);
    	setLocationRelativeTo(null);

    	setLayout(null);
    	getContentPane().setLayout(null);
    	setResizable(false);
    	
    	mainStar = new Character(this, 240, 240);
    	JLabel j = mainStar.getJLable();
    	j.setBounds(mainStar.getAbsoluteX(), mainStar.getAbsoluteY(),Character.blockSize, Character.blockSize);
    	getContentPane().add(j);

    	ss = new SoundSource(this, 480, 480);
    	JLabel jj = ss.getJLable();
    	jj.setBounds(ss.getAbsoluteX(), ss.getAbsoluteY(),Character.blockSize, Character.blockSize );
    	getContentPane().add(jj);

    	ss.playRelativeSound(mainStar);



    	addWindowListener(new AdapterDemo());
    	addKeyListener(mainStar);

    	// SoundSource s = new SoundSource();
    	// Thread t = new Thread(s);
    	// t.start();


      	setVisible(true);
	}


	public void characterWalking(Character c){
		Graphics g = getGraphics();
		super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
    	g2d.drawImage(c.getImage(), c.getAbsoluteX(), c.getAbsoluteY(), c.blockSize, c.blockSize, this);
      	g.dispose();

	}
	public void soundSourceControll(Character c){
		ss.playRelativeSound(c);
	}

}

interface CharacterWalkable{
     public void characterWalking(Character c);
}

interface SoundSourceControllable{
	public void soundSourceControll(Character c);


}



public class Character implements KeyListener, ActionListener{
	private String characterImagePath = "./img/blindman.png";
	private BufferedImage characterImage;
	private javax.swing.Timer timer;
	private JLabel selfJLable;
	private Direction walkingDirection;
	private WalkPose walkingPose;
	private int absoluteX;
	private int absoluteY;

	public static final int xOffset = 96;
	public static final int yOffset = 0;
	public static final int smallIconWidth = 32;
	public static final int smallIconHeight = 32;
	public static final int blockSize = 48;

	private Frame frame;

	@Override
	public void actionPerformed(ActionEvent e) {
		if(timer.isRunning() && e.getActionCommand().equals("moving")){
			if(walkingDirection == Direction.Up){
				absoluteY -= blockSize/3;
			}else if(walkingDirection == Direction.Down){
				absoluteY += blockSize/3;
			}else if(walkingDirection == Direction.Left){
				absoluteX -= blockSize/3;
			}else if(walkingDirection == Direction.Right){
				absoluteX += blockSize/3;
			}

			frame.characterWalking(this);
			if(walkingPose == WalkPose.Still){
				timer.stop();
				selfJLable = new JLabel(new ImageIcon(getImage()));
				selfJLable.setBounds(absoluteX, absoluteY,Character.blockSize, Character.blockSize);
				frame.add(selfJLable);
				frame.repaint();
				frame.soundSourceControll(this);
			}else if(walkingPose == WalkPose.RightLeg){
				walkingPose = WalkPose.LeftLeg;
			}else if(walkingPose == WalkPose.LeftLeg){
				walkingPose = WalkPose.Still;
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e){

		boolean isMovingKeyPressed = true;
		if(!timer.isRunning()){
			if(e.getKeyCode() == KeyEvent.VK_UP){
				walkingDirection = Direction.Up;
			}else if(e.getKeyCode() == KeyEvent.VK_DOWN){
				walkingDirection = Direction.Down;
			}else if(e.getKeyCode() == KeyEvent.VK_LEFT){
				walkingDirection = Direction.Left;
			}else if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				walkingDirection = Direction.Right;
			}else isMovingKeyPressed = false;
			if(isMovingKeyPressed){
				frame.remove(getJLable());
				walkingPose = WalkPose.RightLeg;
				timer.setActionCommand("moving");
				timer.start();
			}
	}
			
		
	}
 

	public Character(Frame f, int x, int y){

		try{
			characterImage = ImageIO.read(new File(characterImagePath));
		}catch(Exception e){
			System.out.println(e);
		}
		frame = f;
		absoluteX = x;
		absoluteY = y;
		timer = new javax.swing.Timer(25, this);
		
		walkingDirection = Direction.Down;
		walkingPose = WalkPose.Still;
		selfJLable = new JLabel(new ImageIcon(getImage()));


	}

	public Image getImage(){
		int x = 0;
		int y = 0;
		if(walkingDirection == Direction.Up){
			y = yOffset + 3*smallIconHeight;
		}else if(walkingDirection == Direction.Left){
			y = yOffset + 1*smallIconHeight;
		}else if(walkingDirection == Direction.Right){
			y = yOffset + 2*smallIconHeight;
		}else if(walkingDirection == Direction.Down){
			y = yOffset + 0*smallIconHeight;
		}
		if(walkingPose == WalkPose.Still){
			x = xOffset + 1*smallIconWidth;
		}else if(walkingPose == WalkPose.LeftLeg){
			x = xOffset + 0*smallIconWidth;
		}else if(walkingPose == WalkPose.RightLeg){
			x = xOffset + 2*smallIconWidth;
		}
		return characterImage.getSubimage(x, y, smallIconWidth, smallIconHeight).getScaledInstance(blockSize, blockSize, java.awt.Image.SCALE_SMOOTH);

	}


	@Override
 	public void keyReleased(KeyEvent e){


  	}
 
 
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	JLabel getJLable(){
		return selfJLable;
	}

	public int getAbsoluteX(){
		return absoluteX;
	}

	public int getAbsoluteY(){
		return absoluteY;
	}

	public static void main(String [] argv){
		Frame frame = new Frame();
		


	}

}
