import java.lang.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.*;

public class SoundSource implements Runnable{
	private String soundSourceImagePath = "./img/flame.png";
	private BufferedImage soundSourceImage;
	private JLabel selfJLable;
	private int absoluteX;
	private int absoluteY;
	private Clip leftClip;
	private Clip rightClip;

	Frame frame;

	public static final int xOffset = 32*10;
	public static final int yOffset = 0;
	public static final int smallIconWidth = 32;
	public static final int smallIconHeight = 32;

	public SoundSource(Frame f, int x, int y){
		try{
			soundSourceImage = ImageIO.read(new File(soundSourceImagePath));
		}catch(Exception e){
			System.out.println(e);
		}

		frame = f;
		absoluteX = x;
		absoluteY = y;
		selfJLable = new JLabel(new ImageIcon(getImage()));

		
	}

	private float determineDistance(Character c){
		return (float)Math.sqrt((c.getAbsoluteX()- absoluteX)*(c.getAbsoluteX()- absoluteX) +  (c.getAbsoluteY()- absoluteY)*(c.getAbsoluteY()- absoluteY)) ;

	}

	private long determineAngle(Character c){
		double theta = Math.atan2((double)(c.getAbsoluteY() - absoluteY), (double)(absoluteX - c.getAbsoluteX()) )* 180 / Math.PI;;
		long degree = Math.round(theta);
		if(degree < 0)degree += 360;
		//now degree is the normal polar coordinate theta
		// going to convert to data specific theda, front-head:0 degree, right hand 90 degrees
		if(degree == 0)degree = 90;
		else if(degree > 0 && degree <= 90)degree = 90 - degree;
		else if(degree > 90 && degree <= 180)degree = (180-degree)+270;
		else if(degree > 180 && degree <= 270)degree = (270-degree)+180;
		else if(degree > 270 && degree < 360)degree = (360 - degree)+ 90;

		while(degree % 6 != 0)degree--;


		return degree;

	}

	public void playRelativeSound(Character c){
		String leftSoundPath = "./fire/left/30_" + determineAngle(c) + ".wav";
		String rightSoundPath = "./fire/right/30_" + determineAngle(c) + ".wav";
		//System.err.println(determineAngle(c));
		try {
		    File leftSource = new File(leftSoundPath);
		    AudioInputStream leftStream = AudioSystem.getAudioInputStream(leftSource);
		    AudioFormat leftFormat = leftStream.getFormat();
		    DataLine.Info leftInfo = new DataLine.Info(Clip.class, leftFormat);

		    File rightSource = new File(rightSoundPath);
		    AudioInputStream rightStream = AudioSystem.getAudioInputStream(rightSource);
		    AudioFormat rightFormat = rightStream.getFormat();
		    DataLine.Info rightInfo = new DataLine.Info(Clip.class, rightFormat);

		    int framePosition = 0;
			if(leftClip != null){
				framePosition = leftClip.getFramePosition();
				leftClip.close();
				leftClip = null;
			}
			if(rightClip != null){
				rightClip.close();
				rightClip = null;
			}
		    
		    leftClip = (Clip) AudioSystem.getLine(leftInfo);
		    leftClip.open(leftStream);
		    leftClip.setFramePosition(framePosition);
		    FloatControl leftGainControl = (FloatControl) leftClip.getControl(FloatControl.Type.MASTER_GAIN);
		    float dec = (determineDistance(c));
			leftGainControl.setValue(-25.0f*dec/600.0f); // Reduce volume by 10 dec

			rightClip = (Clip) AudioSystem.getLine(rightInfo);
		    rightClip.open(rightStream);
		    rightClip.setFramePosition(framePosition);
		    FloatControl rightGainControl = (FloatControl) rightClip.getControl(FloatControl.Type.MASTER_GAIN);
			rightGainControl.setValue(-25.0f*dec/600.0f); // Reduce volume by 10 dec

		    leftClip.loop(Clip.LOOP_CONTINUOUSLY);
		    rightClip.loop(Clip.LOOP_CONTINUOUSLY);
		    //clip.start();
		    //System.err.println(dec);
		}
		catch (Exception e) {
			System.err.println(e);
		}

	}

	public JLabel getJLable(){
		return selfJLable;
	}

	public Image getImage(){
		int x = xOffset + 1*smallIconWidth;
		int y = yOffset + 0*smallIconHeight;
		return soundSourceImage.getSubimage(x, y, smallIconWidth, smallIconHeight).getScaledInstance(Character.blockSize, Character.blockSize, java.awt.Image.SCALE_SMOOTH);

	}

	public int getAbsoluteX(){
		return absoluteX;
	}

	public int getAbsoluteY(){
		return absoluteY;
	}
	public void run(){
		System.err.println("Yo");
	}


}